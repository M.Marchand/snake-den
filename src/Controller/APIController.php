<?php

namespace App\Controller;

use App\Repository\SnakeFamilyRepository;
use App\Repository\SnakeRepository;
use App\Repository\SpeciesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;


class APIController extends AbstractController
{
    private $snakeRepository;
    private $speciesRepository;
    private $snakeFamilyRepository;
    public function __construct(SnakeRepository $snakeRepository, SpeciesRepository $speciesRepository, SnakeFamilyRepository $snakeFamilyRepository)
    {
        $this->snakeRepository=$snakeRepository;
        $this->speciesRepository=$speciesRepository;
        $this->snakeFamilyRepository=$snakeFamilyRepository;
    }


    /**
     * @Route("/API/Snake", name="a_p_iS")
     */
    public function snake()
    {
        $arraySnake = $this->snakeRepository->findAll();
        $this->getDoctrine();

        $arraySnake = $this->get('serializer')->serialize($arraySnake, 'json');
//dump($arraySnake);
        return new JsonResponse(
            $arraySnake, 200, [], true
        );

    }
    /**
     * @Route("/API/Species", name="a_p_i")
     */
    public function species()
    {
        $arraySpecies = $this->speciesRepository->findAll();
        $this->getDoctrine();

        $arraySpecies = $this->get('serializer')->serialize($arraySpecies, 'json');

        return new JsonResponse(
            $arraySpecies, 200, [], true
        );

    }
    /**
     * @Route("/API/SnakeFamily", name="api")
     */
    public function snakeFamily()
    {
        $arraySnakeFamily = $this->snakeFamilyRepository->findAll();
        $this->getDoctrine();

        $arraySnakeFamily = $this->get('serializer')->serialize($arraySnakeFamily, 'json');

        return new JsonResponse(
            $arraySnakeFamily, 200, [], true
        );

    }
}
