<?php

namespace App\Controller;

use App\Repository\SnakeFamilyRepository;
use App\Repository\SnakeRepository;
use App\Repository\SpeciesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ViewController extends AbstractController
{
    private $snakeRepository;
    private $speciesRepository;
    private $snakeFamilyRepository;
 public function __construct(SnakeRepository $snakeRepository, SpeciesRepository $speciesRepository, SnakeFamilyRepository $snakeFamilyRepository)
 {
     $this->snakeRepository=$snakeRepository;
     $this->speciesRepository=$speciesRepository;
     $this->snakeFamilyRepository=$snakeFamilyRepository;

 }

    /**
     * @Route("/view/species", name="view/species")
     */
    public function Species()
    {
        $arraySpecie= $this->speciesRepository->findAll();

        return $this->render('view/Species.html.twig', [
            'controller_name' => 'ViewController',
            'specie'=>$arraySpecie,
        ]);
    }
    /**
     * @Route("/view/snake", name="view/snake")
     */
    public function snake()
    {
        $arraySnake= $this->snakeRepository->findAll();

        return $this->render('view/snake.html.twig', [
            'controller_name' => 'ViewController',
            'snake'=>$arraySnake,
        ]);
    }
    /**
     * @Route("/view/snakeFamily", name="view/snakeFamily")
     */
    public function snakeFamily()
    {
        $arraySnakeFamily= $this->snakeFamilyRepository->findAll();

        return $this->render('view/snakeFamily.html.twig', [
            'controller_name' => 'ViewController',
            'snakeFamily'=>$arraySnakeFamily,
        ]);
    }
    /**
     * @Route("/view/accueil", name="view/accueil")
     */
    public function accueil()
    {
        return$this->render('view/accueil.html.twig');
    }
}
