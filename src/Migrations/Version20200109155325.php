<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200109155325 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE species (id INT AUTO_INCREMENT NOT NULL, snake_base_constitution_id INT DEFAULT NULL, scientific_name VARCHAR(255) DEFAULT NULL, usual_name VARCHAR(255) DEFAULT NULL, family VARCHAR(255) DEFAULT NULL, origin VARCHAR(255) DEFAULT NULL, geographic_distribution VARCHAR(255) DEFAULT NULL, nourishment VARCHAR(500) DEFAULT NULL, reproduction_process VARCHAR(500) DEFAULT NULL, measurement VARCHAR(500) DEFAULT NULL, anatomical_structure VARCHAR(1000) DEFAULT NULL, physical_characteristics VARCHAR(800) DEFAULT NULL, predation VARCHAR(800) DEFAULT NULL, etymology VARCHAR(500) DEFAULT NULL, INDEX IDX_A50FF712F3E9E8D6 (snake_base_constitution_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE species ADD CONSTRAINT FK_A50FF712F3E9E8D6 FOREIGN KEY (snake_base_constitution_id) REFERENCES snake (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE species');
    }
}
