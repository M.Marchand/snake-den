<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SnakeRepository")
 */
class Snake
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;



    /**
     * @ORM\Column(type="string", length=4000, nullable=true)
     */
    private $Anatomie;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Species", mappedBy="SnakeBaseConstitution")
     */
    private $species;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $Other;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $Definition;

    public function __construct()
    {
        $this->species = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDefinition(): ?string
    {
        return $this->Definition;
    }

    public function setDefinition(?string $Definition): self
    {
        $this->Definition = $Definition;

        return $this;
    }

    public function getAnatomie(): ?string
    {
        return $this->Anatomie;
    }

    public function setAnatomie(?string $Anatomie): self
    {
        $this->Anatomie = $Anatomie;

        return $this;
    }

    /**
     * @return Collection|Species[]
     */
    public function getSpecies(): Collection
    {
        return $this->species;
    }

    public function addSpecies(Species $species): self
    {
        if (!$this->species->contains($species)) {
            $this->species[] = $species;
            $species->setSnakeBaseConstitution($this);
        }

        return $this;
    }

    public function removeSpecies(Species $species): self
    {
        if ($this->species->contains($species)) {
            $this->species->removeElement($species);
            // set the owning side to null (unless already changed)
            if ($species->getSnakeBaseConstitution() === $this) {
                $species->setSnakeBaseConstitution(null);
            }
        }

        return $this;
    }

    public function getOther(): ?string
    {
        return $this->Other;
    }

    public function setOther(?string $Other): self
    {
        $this->Other = $Other;

        return $this;
    }


}
