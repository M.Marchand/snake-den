<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
/**
 * @ORM\Entity(repositoryClass="App\Repository\SpeciesRepository")
 */

/**
 * @ORM\Entity
 * @Vich\Uploadable
 */
class Species
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ScientificName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $UsualName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Family;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Origin;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $GeographicDistribution;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $Nourishment;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $ReproductionProcess;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $Measurement;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $AnatomicalStructure;

    /**
     * @ORM\Column(type="string", length=800, nullable=true)
     */
    private $PhysicalCharacteristics;

    /**
     * @ORM\Column(type="string", length=800, nullable=true)
     */
    private $Predation;

    /**
     * @ORM\Column(type="string", length=500, nullable=true)
     */
    private $Etymology;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Snake", inversedBy="species")
     */
    private $SnakeBaseConstitution;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $Photo;
    /**
     * @Vich\UploadableField(mapping="Photo", fileNameProperty="Photo")
     * @var File
     */
    private $photoFile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getScientificName(): ?string
    {
        return $this->ScientificName;
    }

    public function setScientificName(?string $ScientificName): self
    {
        $this->ScientificName = $ScientificName;

        return $this;
    }

    public function getUsualName(): ?string
    {
        return $this->UsualName;
    }

    public function setUsualName(?string $UsualName): self
    {
        $this->UsualName = $UsualName;

        return $this;
    }

    public function getFamily(): ?string
    {
        return $this->Family;
    }

    public function setFamily(?string $Family): self
    {
        $this->Family = $Family;

        return $this;
    }

    public function getOrigin(): ?string
    {
        return $this->Origin;
    }

    public function setOrigin(?string $Origin): self
    {
        $this->Origin = $Origin;

        return $this;
    }

    public function getGeographicDistribution(): ?string
    {
        return $this->GeographicDistribution;
    }

    public function setGeographicDistribution(?string $GeographicDistribution): self
    {
        $this->GeographicDistribution = $GeographicDistribution;

        return $this;
    }

    public function getNourishment(): ?string
    {
        return $this->Nourishment;
    }

    public function setNourishment(?string $Nourishment): self
    {
        $this->Nourishment = $Nourishment;

        return $this;
    }

    public function getReproductionProcess(): ?string
    {
        return $this->ReproductionProcess;
    }

    public function setReproductionProcess(?string $ReproductionProcess): self
    {
        $this->ReproductionProcess = $ReproductionProcess;

        return $this;
    }

    public function getMeasurement(): ?string
    {
        return $this->Measurement;
    }

    public function setMeasurement(?string $Measurement): self
    {
        $this->Measurement = $Measurement;

        return $this;
    }

    public function getAnatomicalStructure(): ?string
    {
        return $this->AnatomicalStructure;
    }

    public function setAnatomicalStructure(?string $AnatomicalStructure): self
    {
        $this->AnatomicalStructure = $AnatomicalStructure;

        return $this;
    }

    public function getPhysicalCharacteristics(): ?string
    {
        return $this->PhysicalCharacteristics;
    }

    public function setPhysicalCharacteristics(?string $PhysicalCharacteristics): self
    {
        $this->PhysicalCharacteristics = $PhysicalCharacteristics;

        return $this;
    }

    public function getPredation(): ?string
    {
        return $this->Predation;
    }

    public function setPredation(?string $Predation): self
    {
        $this->Predation = $Predation;

        return $this;
    }

    public function getEtymology(): ?string
    {
        return $this->Etymology;
    }

    public function setEtymology(?string $Etymology): self
    {
        $this->Etymology = $Etymology;

        return $this;
    }

    public function getSnakeBaseConstitution(): ?Snake
    {
        return $this->SnakeBaseConstitution;
    }

    public function setSnakeBaseConstitution(?Snake $SnakeBaseConstitution): self
    {
        $this->SnakeBaseConstitution = $SnakeBaseConstitution;

        return $this;
    }

    public function getPhoto(): ?string
    {
        return $this->Photo;
    }

    public function setPhoto(?string $Photo): self
    {
        $this->Photo = $Photo;
        if ($Photo) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
        return $this;
    }
    public function getphotoFile()
    {
        return $this->photoFile;
    }

    public function setphotoFile(File $Photo = null)
    {
        $this->photoFile = $Photo;


    }
}
