<?php

namespace App\Repository;

use App\Entity\SnakeFamily;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SnakeFamily|null find($id, $lockMode = null, $lockVersion = null)
 * @method SnakeFamily|null findOneBy(array $criteria, array $orderBy = null)
 * @method SnakeFamily[]    findAll()
 * @method SnakeFamily[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SnakeFamilyRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SnakeFamily::class);
    }

    // /**
    //  * @return SnakeFamily[] Returns an array of SnakeFamily objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SnakeFamily
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
