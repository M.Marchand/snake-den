<?php

namespace App\Repository;

use App\Entity\Snake;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Snake|null find($id, $lockMode = null, $lockVersion = null)
 * @method Snake|null findOneBy(array $criteria, array $orderBy = null)
 * @method Snake[]    findAll()
 * @method Snake[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SnakeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Snake::class);
    }

    // /**
    //  * @return Snake[] Returns an array of Snake objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Snake
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
