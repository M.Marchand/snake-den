Snake-Den est une API regroupant diverses espèces de serpents avec leurs caractéristiques, ce projet est encore en cours et reste à alimenter d'informations.
Cette API est réalisée avec le framework Symfony (v.4.4),j'y utilise principalement les bundles easy admin et vichUploader. Le tout étant dockerisé.

**-Les requêtes-**

*  Pour les serpents = API/snakes
Vous trouverez les éléments suivants :
{"id":,"Anatomy":"","Definition":"","Other":}

*  Pour les éspèces = API/species
Vous trouverez les éléments suivants :
[{"id":2,"ScientificName":,"UsualName":","Family":"","Origin":,"GeographicDistribution":"","Nourishment":,"ReproductionProcess":,"Measurement":"","AnatomicalStructure":,"PhysicalCharacteristics":,"Predation":,"Etymology":"","SnakeBaseConstitution":,"Photo":,"photoFile":}

*  Pour les familles de serpents= API/snakeFamily
Vous trouverez les éléments suivants :
[{"id":,"Name":"","Description":"","ScientificName":""}
